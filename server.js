require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT ||3000; // process.env
const URL_BASE = '/techu/v1/';
const userFile = require('./users.json');
const body_parser = require('body-parser');

app.listen (port, function(){
  console.log('Node js escuchando desde el puerto:'+ port);
});

app.use(body_parser.json());

//operacion get status
app.get (URL_BASE + 'users',
    function(request, response){
      response.status(200).send(userFile);
    }
);

//operacion get
app.get(URL_BASE + 'users',
    function(request, response){
      response.send(userFile);
    });
// operacion get
app.get(URL_BASE + 'users/:id_user',
        function(request, response){
        console.log(request.params.id_user);
        let pos = request.params.id_user -1;
        let respuesta = (userFile[pos]== undefined)?
         {"msg":"usuario no existe"} :userFile[pos];
        let statusCode = (userFile[pos]==undefined)? 404:200;
        response.status(statusCode).send(respuesta);
        });

// operacion get con query string
app.get(URL_BASE + 'userq',
  function(req, res){
    console.log(req.query.id);
    console.log(req.query.country);
    res.send({"msg":"GET con query"});
  });

//operacion post
app.post(URL_BASE + 'users',
  function(req, res){
    console.log('post a users');
    let tam = userFile.length;
    let new_user = {
      "id_user": tam + 1,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "gender" : req.body.gender,
      "ip_address" : req.body.ip_address,
    }
    console.log(new_user);
    userFile.push(new_user);
    res.send({"msg":"Usuario creado correctamente"});
  });

// operacion put
app.put(URL_BASE + 'users/:id',
  function(req, res){
    console.log('put a users');
    let idBuscar = req.params.id;
    let mod_user = {
      "id_user" :   req.params.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "gender" : req.body.gender,
      "ip_address" : req.body.ip_address,
    }
    for(i = 0; i < userFile.length; i++) {
       if(userFile[i].id_user == idBuscar) {
         userFile[i]= mod_user;
         res.send({"msg" : "Usuario actualizado correctamente.", mod_user});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
  });
// operacion delete
app.delete(URL_BASE + 'users/:id',
    function(req, res){
    let idBuscar = req.params.id;
    for(i = 0; i < userFile.length; i++) {
       if(userFile[i].id_user == idBuscar) {
         userFile.splice(i,1);
         res.send({"msg" : "Usuario eliminado correctamente."});
         console.log(userFile);
       }
     }
     res.status(404).send({"msg" : "Usuario no encontrado."});
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of userFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(userFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : us.logged });
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
          response.send({"msg":"Login incorrecto"});
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }

   // LOGOUT - users.json
   app.post(URL_BASE + 'logout',
     function(request, response) {
       console.log("POST /apicol/v2/logout");
       var userId = request.body.id_user;
       for(us of userFile) {
         if(us.id_user == userId) {
           if(us.logged) {
             delete us.logged; // borramos propiedad 'logged'
             writeUserDataToFile(userFile);
             console.log("Logout correcto!");
             response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
           } else {
             console.log("Logout incorrecto.");
             response.send({"msg" : "Logout incorrecto."});
           }
          }
        }
            response.send({"msg" :"Fallo el logout"});
        });
    // Cantidad de usuarios
    app.get(URL_BASE + 'totalUsers',
      function(request, response) {
             response.send({"num_usuarios" : userFile.length });
         });
